class Scales:
    def __str__(self):
        return f'max allowed weight is {self.max_weight}'

    def __init__(self, name):
        self.max_weight = name
        self.left = 0
        self.right = 0

    def __call__(self):
        if int(self.left) > int(self.right):
            return 'L'
        if int(self.right) > int(self.left):
            return 'R'
        if int(self.right) == int(self.left):
            return '='

    def add_left(self, value):
        if (self.left + value) > self.max_weight:
            return print('value is too heavy')
        self.left += value

    def add_right(self, value):
        if (self.right + value) > self.max_weight:
            return print('value is too heavy')
        self.right += value

    def remove_items(self):
        self.left = 0
        self.right = 0
